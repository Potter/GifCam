#define ANALOG_PIN 0
#define LED_PIN 13
#define OUT_1 6
#define OUT_2 7
#define OUT_3 8
#define OUT_4 9

long val = 0;
int Steps = 0;
boolean isBluetooth = false;
char tmp_str[20] = "";
char func_name[20] = "";
char chr;
int i=0;
int argv[10]; // 0 - argc, 1..9 - argv
boolean clockwise = true;
boolean ledStatus = true;
int stepDelayTime = 10;



          
void setup() {
    Serial.begin(9600);
    pinMode(OUT_1, OUTPUT); 
    pinMode(OUT_2, OUTPUT); 
    pinMode(OUT_3, OUTPUT); 
    pinMode(OUT_4, OUTPUT); 
    pinMode(ANALOG_PIN, INPUT); 
    pinMode(LED_PIN, OUTPUT); 
}

void loop() { 
    if (isBluetooth) {
        if (Serial.available()) {
            for (i = 0; i < 20; i++) {
                func_name[i] = 0;
                tmp_str[i] = 0;
            }
            for (i = 0; i < 10; i++) {
                argv[i] = 0;
            }
            i = 0;
            delay(100);
            while (Serial.available()) {
                ledStatus = !ledStatus;
                chr = Serial.read();
                if (chr != ' ') {
                    tmp_str[i] = chr;
                    i++;    
                } else {
                    if (argv[0] == 0) {
                        for (i = 0; i < 20; i++) {
                            func_name[i] = tmp_str[i];
                            tmp_str[i] = 0;
                        }
                    } else {
                        argv[argv[0]] = atoi(tmp_str);
                        for (i = 0; i < 20; i++) {
                            tmp_str[i] = 0;
                        }
                    }
                    argv[0]++;
                    i = 0;
                }
            }           
            Serial.flush();

            if (str_equal(func_name, "cw")) {
                clockwise = true;                
                doSteps(argv[1], argv[2]);
            }
            if (str_equal(func_name, "ccw")) {
                clockwise = false;
                doSteps(argv[1], argv[2]);
            }
            
            if (ledStatus) {
                digitalWrite(LED_PIN, HIGH);
            } else {
                digitalWrite(LED_PIN, LOW);
            }
        }
    
    } else {
      
        ledStatus = !ledStatus;
        if (ledStatus) {
            digitalWrite(LED_PIN, HIGH);
        } else {
            digitalWrite(LED_PIN, LOW);
        }
        delay(100);
      
        if (Serial.available()) {
            isBluetooth = true;
        } else {
            int strange = 0;  
            for (int i=0; i < 50; i++) {
                val = analogRead(ANALOG_PIN);
                if (val > 5 && val < 40) {
                    strange++;
                }
            }    
            if (strange > 10) {
                doStep(0);     
                digitalWrite(LED_PIN, HIGH);
                delay(80);
                digitalWrite(LED_PIN, LOW); 
                Serial.println("signal");
                Serial.println(strange);
            }
            Serial.println(analogRead(ANALOG_PIN));
        }
    }
}

boolean str_equal(char * str1, char * str2) {
  for (i = 0; i < 20; i++) {
    if (str1[i] == 0) {
      return true;
    }
    if (str1[i] != str2[i]) {
      return false;
    }
  }
  Serial.println("");
  return true;
}

void doSteps(int count, int halfstep) {
  for (i = 0; i < count; i++) {
    doStep(halfstep);
  }
}


void doStep(int halfstep) {
    switch(Steps) {
        case 0: {
            digitalWrite(OUT_1,HIGH);
            digitalWrite(OUT_2,LOW);
            digitalWrite(OUT_3,LOW);
            digitalWrite(OUT_4,HIGH);
        } break; 
        case 1: {
            digitalWrite(OUT_1,LOW);
            digitalWrite(OUT_2,LOW);
            digitalWrite(OUT_3,LOW);
            digitalWrite(OUT_4,HIGH);
        } break; 
        case 2: {
            digitalWrite(OUT_1,LOW);
            digitalWrite(OUT_2,HIGH);
            digitalWrite(OUT_3,LOW);
            digitalWrite(OUT_4,HIGH);
        } break; 
        case 3: {
            digitalWrite(OUT_1,LOW);
            digitalWrite(OUT_2,HIGH);
            digitalWrite(OUT_3,LOW);
            digitalWrite(OUT_4,LOW);
        } break; 
        case 4: {
            digitalWrite(OUT_1,LOW);
            digitalWrite(OUT_2,HIGH);
            digitalWrite(OUT_3,HIGH);
            digitalWrite(OUT_4,LOW);
        } break; 
        case 5: {
            digitalWrite(OUT_1,LOW);
            digitalWrite(OUT_2,LOW);
            digitalWrite(OUT_3,HIGH);
            digitalWrite(OUT_4,LOW);
        } break; 
        case 6: {
            digitalWrite(OUT_1,HIGH);
            digitalWrite(OUT_2,LOW);
            digitalWrite(OUT_3,HIGH);
            digitalWrite(OUT_4,LOW);
        } break; 
        case 7: {
            digitalWrite(OUT_1,HIGH);
            digitalWrite(OUT_2,LOW);
            digitalWrite(OUT_3,LOW);
            digitalWrite(OUT_4,LOW);
        } break; 
        default: {
            digitalWrite(OUT_1, LOW); 
            digitalWrite(OUT_2, LOW);
            digitalWrite(OUT_3, LOW);
            digitalWrite(OUT_4, LOW);
        } break; 
    }
    
    if (clockwise) {
        if (halfstep != 1) {
          Steps++;
        }
        Steps++;
        if (Steps > 7) {
            Steps=0;
        }
    } else {
        if (halfstep != 1) {
          Steps--;
        }
        Steps--;
        if (Steps < 0) {
            Steps=7;
        }    
    }
    delay(stepDelayTime);
}



