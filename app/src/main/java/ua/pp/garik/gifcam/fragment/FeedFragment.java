package ua.pp.garik.gifcam.fragment;

import org.apache.commons.io.FileUtils;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.adapter.PostAdapter;
import ua.pp.garik.gifcam.adapter.SmoothScrollLayoutManager;
import ua.pp.garik.gifcam.api.ApiHelper;
import ua.pp.garik.gifcam.api.GifCamService;
import ua.pp.garik.gifcam.entity.User;
import ua.pp.garik.gifcam.entity.Video;
import ua.pp.garik.gifcam.utils.ConnectionChecker;
import ua.pp.garik.gifcam.utils.FeedContainer;
import ua.pp.garik.gifcam.utils.Utils;
import ua.pp.garik.gifcam.utils.VideoContainer;

/**
 * Created by Vova on 01.03.2015.
 */
public class FeedFragment extends Fragment implements PostAdapter.OnPostRowSelectedListener {
    public static final int MODE_MY = 0;
    public static final int MODE_GLOBAL = 1;
    public static final int MODE_FAVOURITE = 2;
    public static final int MODE_USER = 3;
    private static final String MODE_ID = "mode";
    private static final String USER_ID = "user";
    private RecyclerView mFeedView;
    private ProgressBar mProgressBar;
    private PostAdapter mAdapter;
    private SmoothScrollLayoutManager mLayoutManager;
    private OnPostSelectedListener mListener;
    private User mUser;
    private InputStream mGifStream;
    public static FeedFragment newInstance(int mode, User user) {
        FeedFragment feedFragment = new FeedFragment();
        Bundle args = new Bundle();
        args.putInt(MODE_ID, mode);
        if (user != null) {
            args.putSerializable(USER_ID, user);
        }
        feedFragment.setArguments(args);
        return feedFragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feed, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mFeedView = (RecyclerView) view.findViewById(R.id.feed);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_indicator);
        mFeedView.setHasFixedSize(true);
        mLayoutManager =
            new SmoothScrollLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false, 7000);
        mFeedView.setLayoutManager(mLayoutManager);
        mFeedView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new PostAdapter(getActivity(), this);
        mFeedView.setAdapter(mAdapter);
        Bundle args = getArguments();
        int mode = args.getInt(MODE_ID);
        switch (mode) {
            case MODE_GLOBAL:
                (new GetFeedTask(getActivity())).execute(null, null);
                break;
            case MODE_MY:
                (new GetUserVideosTask(getActivity())).execute();
                break;
            case MODE_FAVOURITE:
                (new GetFavouriteVideosTask(getActivity())).execute();
                break;
            case MODE_USER:
                User user = (User) args.getSerializable(USER_ID);
                mUser = new User();
                mUser.setId(user.getId());
                (new GetUserVideosTask(getActivity())).execute(mUser);
                break;
        }
    }

    @Override
    public void onUserLogoClicked(int position) {
        mListener.onPostAuthorSelected(mAdapter.getItem(position).getUser());
    }

    @Override
    public void onFavoriteClicked(int position, PostAdapter.ViewHolder holder) {
        (new FavouriteTask(holder, getActivity())).execute(mAdapter.getItem(position));
    }

    @Override
    public void onPreviewClicked(int position, final PostAdapter.ViewHolder viewHolder) {
        Video video = mAdapter.getItem(position);
        String extension = video.getPath().substring(video.getPath().lastIndexOf("."));
        if (extension.equals(".gif")) {
            (new LoadGifFromServerToView(viewHolder)).execute(video);

        } else if (extension.equals(".mp4")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri videoUri = Uri.parse(Utils.BASE_URL + video.getPath());
            intent.setDataAndType(videoUri, "video/mp4");
            startActivity(intent);
        }
    }
    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPostSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement OnPostSelectedListener interface");
        }
    }

    private User getMyUser() {
        Realm realm = Realm.getInstance(getActivity());
        RealmResults<User> users = realm.where(User.class).equalTo("mode", User.PERSONAL_MODE)
            .findAll();
        if (users.size() > 0) {
            return users.first();
        }
        return null;
    }

    public interface OnPostSelectedListener {
        public void onPostAuthorSelected(User user);
    }

    private class GetFeedTask extends AsyncTask<Long, Void, List<Video>> {
        private Activity mContext;

        private GetFeedTask(Activity context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
            mFeedView.setVisibility(View.GONE);
        }

        @Override
        protected List<Video> doInBackground(Long... params) {
            ApiHelper helper = new ApiHelper(mContext);
            GifCamService service = helper.getService();
            return service.getVideoFeed(params[0], params[1]).getVideos();
        }

        @Override
        protected void onPostExecute(List<Video> videos) {
            super.onPostExecute(videos);
            mProgressBar.setVisibility(View.GONE);
            mFeedView.setVisibility(View.VISIBLE);
            mAdapter.addData(videos);
        }
    }

    private class GetUserVideosTask extends AsyncTask<User, Void, List<Video>> {
        private Activity mContext;

        private GetUserVideosTask(Activity context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<Video> doInBackground(User... params) {
            long userId;
            if (params.length == 0) {
                userId = getMyUser().getId();
            } else {
                userId = params[0].getId();
            }
            FeedContainer container = (new ApiHelper(mContext))
                .getService().getUserVideos(userId);
            return container.getVideos();
        }

        @Override
        protected void onPostExecute(List<Video> videos) {
            super.onPostExecute(videos);
            mProgressBar.setVisibility(View.GONE);
            mAdapter.addData(videos);
        }

    }

    private class GetFavouriteVideosTask extends AsyncTask<Void, Void, List<Video>> {
        private Activity mContext;
        private ConnectionChecker mConnectionChecker;
        private GetFavouriteVideosTask(Activity context) {
            mContext = context;
            mConnectionChecker = new ConnectionChecker(mContext);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<Video> doInBackground(Void... params) {
            if (!mConnectionChecker.isNetworkAvailable()) {
                Realm realm = Realm.getInstance(mContext);
                RealmResults<Video> videoRealmResults = realm.where(Video.class)
                    .equalTo("mode", Video.MODE_FAVOURITE).findAll();
                List<Video> videos = new ArrayList<>();
                for (Video video : videoRealmResults) {
                    videos.add(video);
                }
                return videos;
            } else {
                FeedContainer container = (new ApiHelper(mContext)).getService().getUserFavorites(0);
                return container.getVideos();
            }


        }
        @Override
        protected void onPostExecute(List<Video> videos) {
            super.onPostExecute(videos);
            mProgressBar.setVisibility(View.GONE);
            mAdapter.addData(videos);
        }
    }

    private class LoadGifFromServerToView extends AsyncTask<Video, Void, InputStream> {
        private PostAdapter.ViewHolder mHolder;

        private LoadGifFromServerToView(PostAdapter.ViewHolder holder) {
            mHolder = holder;
        }

        @Override
        protected InputStream doInBackground(Video... params) {
            File filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File file = new File(filePath, "GIF" + params[0].getId() + ".gif");
            try {
                FileUtils.copyURLToFile(new URL(Utils.BASE_URL + params[0].getPath()), file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                return new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            super.onPostExecute(inputStream);
            if (inputStream != null) {
                mHolder.mGIFView.setVisibility(View.VISIBLE);
                mHolder.mGifPreviewImageView.setVisibility(View.GONE);
                mHolder.mGIFView.playGif(inputStream);

            }
        }

    }

    private class FavouriteTask extends AsyncTask<Video, Void, Video> {
        private PostAdapter.ViewHolder mHolder;
        private Activity mContext;

        private FavouriteTask(PostAdapter.ViewHolder holder, Activity context) {
            mHolder = holder;
            mContext = context;
        }

        @Override
        protected Video doInBackground(Video... params) {
            if (params[0].isRated() == 0) {
                VideoContainer container = (new ApiHelper(mContext)).getService()
                    .favouriteVideo(params[0].getId());
                Video video = container.getVideo();
                Realm realm = Realm.getInstance(mContext);
                realm.beginTransaction();
                video.setMode(Video.MODE_FAVOURITE);
                realm.copyToRealmOrUpdate(video);
                realm.commitTransaction();
                return container.getVideo();
            } else if (params[0].isRated() == 1) {
                VideoContainer container = (new ApiHelper(mContext)).getService()
                    .unFavouriteVideo(params[0].getId());
                Video video = container.getVideo();
                Realm realm = Realm.getInstance(mContext);
                RealmResults<Video> videos = realm.where(Video.class)
                    .equalTo("id", video.getId())
                    .equalTo("mode", Video.MODE_FAVOURITE)
                    .findAll();
                if (videos.size() > 0) {
                    realm.beginTransaction();
                    videos.clear();
                    realm.commitTransaction();
                } else {
                    video.setIsRated(0);
                    return video;
                }

                return container.getVideo();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Video video) {
            super.onPostExecute(video);
            mAdapter.setFavoriteButtonState(video, mHolder);
            mAdapter.updateList(mHolder, video);
        }
    }
}
