package ua.pp.garik.gifcam.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.adapter.UsersAdapter;
import ua.pp.garik.gifcam.api.ApiHelper;
import ua.pp.garik.gifcam.entity.User;
import ua.pp.garik.gifcam.utils.FollowersContainer;
import ua.pp.garik.gifcam.utils.FollowingContainer;

/**
 * Created by Vova on 03.04.2015.
 */
public class UsersFragment extends Fragment implements UsersAdapter.OnUserRowSelectedListener {
    public static final int MODE_FOLLOWERS = 0;
    public static final int MODE_FOLLOWING = 1;
    public static final String MODE_ID = "mode";
    public static final String USER_ID = "user";
    private RecyclerView mUsersView;
    private UsersAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private OnUserSelectedListener mListener;
    private User mUser;

    public UsersFragment() {
    }

    public static UsersFragment newInstance(int mode, User user) {
        UsersFragment usersFragment = new UsersFragment();
        Bundle args = new Bundle();
        args.putInt(MODE_ID, mode);
        args.putSerializable(USER_ID, user);
        usersFragment.setArguments(args);
        return usersFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_users, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUsersView = (RecyclerView) view.findViewById(R.id.users);
        mUsersView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mUsersView.setLayoutManager(mLayoutManager);
        mAdapter = new UsersAdapter(this);
        mUsersView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        int mode = args.getInt(MODE_ID);
        User user = (User) args.getSerializable(USER_ID);
        mUser = new User();
        mUser.setId(user.getId());
        switch (mode) {
            case MODE_FOLLOWERS:
                if (user != null) {
                    (new GetFollowersTask(getActivity())).execute(mUser);
                }
                break;
            case MODE_FOLLOWING:
                if (user != null) {
                    (new GetFollowingTask(getActivity())).execute(mUser);
                }
                break;
        }
    }

    @Override
    public void onUserItemSelected(int position) {
        mListener.onUserSelected(mAdapter.getItem(position));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnUserSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement a OnUserSelectedListener interface");
        }
    }

    public interface OnUserSelectedListener {
        public void onUserSelected(User user);
    }

    private class GetFollowersTask extends AsyncTask<User, Void, List<User>> {
        private Activity mContext;

        private GetFollowersTask(Activity context) {
            mContext = context;
        }

        @Override
        protected List<User> doInBackground(User... params) {
            User user = params[0];
            FollowersContainer container = (new ApiHelper(mContext)).getService()
                .getUserFollowers(user.getId());
            return container.getFollowers();
        }

        @Override
        protected void onPostExecute(List<User> users) {
            super.onPostExecute(users);
            mAdapter.addData(users);
        }
    }

    private class GetFollowingTask extends AsyncTask<User, Void, List<User>> {
        private Activity mContext;

        private GetFollowingTask(Activity context) {
            mContext = context;
        }

        @Override
        protected List<User> doInBackground(User... params) {
            User user = params[0];
            FollowingContainer container = (new ApiHelper(mContext)).getService()
                .getUserFollowing(user.getId());
            return container.getFollowing();
        }

        @Override
        protected void onPostExecute(List<User> users) {
            super.onPostExecute(users);
            mAdapter.addData(users);
        }
    }
}
