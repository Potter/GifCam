package ua.pp.garik.gifcam.adapter;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.entity.Video;
import ua.pp.garik.gifcam.utils.RoundedTransformation;
import ua.pp.garik.gifcam.utils.Utils;
import ua.pp.garik.gifcam.view.GIFView;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {
    private List<Video> mVideos;

    private OnPostRowSelectedListener mListener;
    private Context mContext;
    private Geocoder mGeocoder;
    public PostAdapter(Context context, OnPostRowSelectedListener listener) {
        mVideos = new ArrayList<>();
        mListener = listener;
        mContext = context;
        mGeocoder = new Geocoder(mContext, Locale.ENGLISH);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.post_layout, parent, false);
        return new ViewHolder(view, mListener);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Video video = mVideos.get(position);
        Picasso.with(holder.mUserLogoImageView.getContext()).load(Utils.BASE_URL + video.getUser().getPhoto()).transform(new RoundedTransformation(50, 4)).resize(100, 100).centerCrop().into(holder.mUserLogoImageView);
        holder.mTitleTextView.setText(video.getTitle());
        Picasso.with(holder.mGifPreviewImageView.getContext()).load(Utils.BASE_URL + video.getThumb()).fit().centerCrop().into(holder.mGifPreviewImageView);
        holder.mDescriptionTextView.setText(video.getDescription());
        holder.mPhotosCountTextView.setText(Long.toString(video.getShotsCount()) + " shots");
        setFavoriteButtonState(video, holder);
        holder.mFavoriteCount.setText(Long.toString(video.getRating()));

//        holder.mDurationTextView.setText(Long.toString(video.getDuration()));

        (new LocationLoader(holder)).execute(mGeocoder, video);

    }

    @Override
    public int getItemCount() {
        return mVideos.size();
    }
    public void addData(List<Video> videos) {
        for (int i = 0; i < videos.size(); i++) {
            Video video = videos.get(i);
            mVideos.add(video);
            notifyItemInserted(i);
        }
    }

    public Video getItem(int position) {
        return mVideos.get(position);
    }

    public void setFavoriteButtonState(Video video, ViewHolder holder) {
        if (video.isRated() == 1) {
            holder.mFavoriteView.setBackground(mContext.getResources().getDrawable(R.drawable.fav_bg));
            holder.mFavoriteStar.setBackground(mContext.getResources().getDrawable(R.drawable.ic_star));
            holder.mFavoriteCount.setTextColor(mContext.getResources().getColor(R.color.favotiteText));
            holder.mFavoriteCount.setText(Long.toString(video.getRating()));
        } else if (video.isRated() == 0) {
            holder.mFavoriteView.setBackground(mContext.getResources().getDrawable(R.drawable.unfav_bg));
            holder.mFavoriteStar.setBackground(mContext.getResources().getDrawable(R.drawable.ic_unstar));
            holder.mFavoriteCount.setTextColor(mContext.getResources().getColor(R.color.unFavotiteText));
            holder.mFavoriteCount.setText(Long.toString(video.getRating()));

        }

    }

    public void updateList(ViewHolder holder, Video video) {
        mVideos.set(holder.getPosition(), video);
        notifyItemChanged(holder.getPosition());
    }

    public interface OnPostRowSelectedListener {
        public void onUserLogoClicked(int position);

        public void onFavoriteClicked(int position, ViewHolder holder);

        public void onPreviewClicked(int position, ViewHolder holder);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView mUserLogoImageView;
        public TextView mTitleTextView;
        //        public TextView mLikesCountTextView;
        public ImageView mGifPreviewImageView;
        public TextView mDescriptionTextView;
        public TextView mPhotosCountTextView;
        public LinearLayout mFavoriteView;
        public ImageView mFavoriteStar;
        public TextView mFavoriteCount;
        public TextView mLocationTextView;
        public GIFView mGIFView;
        public VideoView mVideoView;
        private OnPostRowSelectedListener mListener;

        public ViewHolder(View itemView, OnPostRowSelectedListener listener) {
            super(itemView);
            mUserLogoImageView = (ImageView) itemView.findViewById(R.id.user_logo);
            mTitleTextView = (TextView) itemView.findViewById(R.id.title);
//            mLikesCountTextView = (TextView) itemView.findViewById(R.id.likes_count);
            mGifPreviewImageView = (ImageView) itemView.findViewById(R.id.gif_preview);
            mDescriptionTextView = (TextView) itemView.findViewById(R.id.description);
            mPhotosCountTextView = (TextView) itemView.findViewById(R.id.photos);
            mLocationTextView = (TextView) itemView.findViewById(R.id.location);
            mFavoriteView = (LinearLayout) itemView.findViewById(R.id.favourite);
            mFavoriteStar = (ImageView) itemView.findViewById(R.id.favourite_star);
            mFavoriteCount = (TextView) itemView.findViewById(R.id.likes_count);
            mListener = listener;
            mUserLogoImageView.setOnClickListener(this);
            mFavoriteView.setOnClickListener(this);
            mGifPreviewImageView.setOnClickListener(this);
            mGIFView = (GIFView) itemView.findViewById(R.id.gif);
            mVideoView = (VideoView) itemView.findViewById(R.id.video);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.user_logo:
                    mListener.onUserLogoClicked(getPosition());
                    break;
                case R.id.favourite:
                    mListener.onFavoriteClicked(getPosition(), this);
                    break;
                case R.id.gif_preview:
                    mListener.onPreviewClicked(getPosition(), this);
                    break;
            }
        }
    }

    private class LocationLoader extends AsyncTask<Object, Void, String> {
        private ViewHolder mHolder;

        private LocationLoader(ViewHolder holder) {
            mHolder = holder;
        }

        @Override
        protected String doInBackground(Object... params) {
            Geocoder geocoder = (Geocoder) params[0];
            Video video = (Video) params[1];
            if ((video.getLatitude() != null) && (video.getLongitude() != null)) {
                double latitude = Double.parseDouble(video.getLatitude());
                double longitude = Double.parseDouble(video.getLongitude());
                if ((latitude != 0) && (longitude != 0)) {
                try {
                    List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    Address address = addresses.get(0);
                    return "Location: " + address.getAddressLine(0) + ", "
                        + address.getAddressLine(1) + ", " + address.getAddressLine(2);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {
                mHolder.mLocationTextView.setText(s);
            }
        }
    }

}
