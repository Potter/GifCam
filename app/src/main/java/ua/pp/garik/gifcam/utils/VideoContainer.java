package ua.pp.garik.gifcam.utils;

import ua.pp.garik.gifcam.entity.Video;

/**
 * Created by Vova on 23.03.2015.
 */
public class VideoContainer {
    private Video video;

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }
}
