package ua.pp.garik.gifcam.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.io.File;

import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.asynctask.CreateGif;
import ua.pp.garik.gifcam.asynctask.CreateMp4;
import ua.pp.garik.gifcam.entity.Video;

public class SaveFragment extends Fragment {
    private static final String ARG_OUPPUT_FORMAT = "outputformat";
    private static final String ARG_MEDIA_STORAGE_DIR = "mediaStorageDir";
    private static final String ARG_SESSION_LENGTH = "sessionLength";
    Button btn_save;
    EditText et_title, et_description;
    Switch sw_kill_source, sw_upload, sw_location;
    Location location;
    private String outputformat;
    private String mediaStorageDir;
    private int sessionLength;
    private LocationManager mLocationManager = null;
//    private OnFragmentInteractionListener mListener;

    public SaveFragment() {
        // Required empty public constructor
    }

    public static SaveFragment newInstance(String outputformat, String mediaStorageDir, int sessionLength) {
        SaveFragment fragment = new SaveFragment();
        Bundle args = new Bundle();
        args.putString(ARG_OUPPUT_FORMAT, outputformat);
        args.putString(ARG_MEDIA_STORAGE_DIR, mediaStorageDir);
        args.putInt(ARG_SESSION_LENGTH, sessionLength);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            outputformat = getArguments().getString(ARG_OUPPUT_FORMAT);
            mediaStorageDir = getArguments().getString(ARG_MEDIA_STORAGE_DIR);
            sessionLength = getArguments().getInt(ARG_SESSION_LENGTH);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_save, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        et_title = (EditText) view.findViewById(R.id.et_title);
        et_description = (EditText) view.findViewById(R.id.et_description);
        sw_kill_source = (Switch) view.findViewById(R.id.switch_kill_source);
        sw_upload = (Switch) view.findViewById(R.id.switch_upload_to_server);
        sw_location = (Switch) view.findViewById(R.id.switch_location);
        btn_save = (Button) view.findViewById(R.id.btn_save);
        if (savedInstanceState == null) {
            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Video video = new Video();
                    video.setTitle(et_title.getText().toString());
                    video.setDescription(et_description.getText().toString());
                    video.setRating(0);
                    if (sw_location.isChecked()) {
                        if (location == null) {
                            location = getLastBestLocation();
                        }
                        if (location != null) {
                            video.setLatitude(String.valueOf(location.getLatitude()));
                            video.setLongitude(String.valueOf(location.getLongitude()));
                        } else {
                            video.setLatitude("30");
                            video.setLongitude("40");
                        }
                    }
                    video.setDuration(0);
                    video.setShotsCount(sessionLength);


                    File storageDir = new File(mediaStorageDir);
                    if (!storageDir.exists()) {
                        storageDir.mkdirs();
                    }
                    switch (outputformat) {
                        case "gif": {
                            new CreateGif(getActivity()).execute(storageDir, video, sw_kill_source.isChecked(), sw_upload.isChecked(), btn_save);
                        }
                        break;
                        case "mp4": {
                            new CreateMp4(getActivity()).execute(storageDir, video, sw_kill_source.isChecked(), sw_upload.isChecked(), btn_save);
                        }
                        break;
                    }

                    btn_save.setText("Saving...");
                    btn_save.setOnClickListener(null);
                }
            });
        }




        mLocationManager = (LocationManager) getActivity().getSystemService(Activity.LOCATION_SERVICE);
    }


//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }
//
//
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        public void onFragmentInteraction(Uri uri);
//    }


    private Location getLastBestLocation() {
        Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) {
            GPSLocationTime = locationGPS.getTime();
        }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if (0 < GPSLocationTime - NetLocationTime) {
            return locationGPS;
        } else {
            return locationNet;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}
