package ua.pp.garik.gifcam.utils;

import java.util.List;

import ua.pp.garik.gifcam.entity.User;

/**
 * Created by Vova on 03.04.2015.
 */
public class FollowingContainer {
    private List<User> following;

    public List<User> getFollowing() {
        return following;
    }

    public void setFollowing(List<User> following) {
        this.following = following;
    }
}
