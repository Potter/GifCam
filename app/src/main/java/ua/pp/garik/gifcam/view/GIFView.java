package ua.pp.garik.gifcam.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.io.InputStream;

import ua.pp.garik.gifcam.utils.GifDecoder;

/**
 * Created by Vova on 05.04.2015.
 */
public class GIFView extends ImageView {
    final Handler mHandler = new Handler();
    private InputStream mInputStream;
    private boolean mIsPlayingGif = false;
    private GifDecoder mGifDecoder;
    private Bitmap mTmpBitmap;
    final Runnable mUpdateResults = new Runnable() {
        @Override
        public void run() {
            if (mTmpBitmap != null && !mTmpBitmap.isRecycled()) {
                GIFView.this.setImageBitmap(mTmpBitmap);
            }
        }
    };

    public GIFView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void playGif(InputStream stream) {
        mGifDecoder = new GifDecoder();
        mGifDecoder.read(stream);
        mIsPlayingGif = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                final int n = mGifDecoder.getFrameCount();
                final int nTimes = mGifDecoder.getLoopCount();
                int repetitionCounter = 0;
                do {
                    for (int i = 0; i < n; i++) {
                        mTmpBitmap = mGifDecoder.getFrame(i);
                        final int t = mGifDecoder.getDelay(i);
                        mHandler.post(mUpdateResults);
                        try {
                            Thread.sleep(t);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (nTimes != 0) {
                        repetitionCounter++;
                    }
                }
                while (mIsPlayingGif && (repetitionCounter <= nTimes));
            }
        }).start();
    }

    public void setInputStream(InputStream inputStream) {
        mInputStream = inputStream;
        playGif(mInputStream);
    }
}
