package ua.pp.garik.gifcam.utils;

import ua.pp.garik.gifcam.entity.User;

/**
 * Created by Vova on 22.03.2015.
 */
public class RegistrationContainer {
    private User user;
    private String apikey;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }
}
