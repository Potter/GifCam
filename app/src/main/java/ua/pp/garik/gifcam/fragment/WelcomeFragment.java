package ua.pp.garik.gifcam.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ua.pp.garik.gifcam.R;

/**
 * Created by Vova on 12.04.2015.
 */
public class WelcomeFragment extends Fragment implements View.OnClickListener {
    private OnWelcomeInformationClickedListener mListener;

    public WelcomeFragment() {
    }

    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.please_login:
                mListener.onPleaseLoginClicked();
                break;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_welcome, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.please_login).setOnClickListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnWelcomeInformationClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement "
                    + "OnWelcomeInformationClickedListener interface");
        }
    }

    public interface OnWelcomeInformationClickedListener {
        public void onPleaseLoginClicked();
    }
}