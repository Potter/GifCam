package ua.pp.garik.gifcam.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.preferences.DarkThemeEditTextPreference;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settings_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(android.R.id.list).setBackgroundColor
            (getActivity().getResources().getColor(R.color.preference_background));
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().
            registerOnSharedPreferenceChangeListener(this);
        setSummary(findPreference("file_prefix"),
            getPreferenceManager().getSharedPreferences().getString("file_prefix", "File prefix"));

    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().
            unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        setSummary(findPreference("file_prefix"));
    }

    private void setSummary(Preference preference, String summary) {

        if (preference instanceof DarkThemeEditTextPreference) {
            DarkThemeEditTextPreference editTextPreference = (DarkThemeEditTextPreference) preference;
            preference.setSummary(summary);
        }
    }

    private void setSummary(Preference preference) {
        if (preference instanceof DarkThemeEditTextPreference) {
            DarkThemeEditTextPreference editTextPreference = (DarkThemeEditTextPreference) preference;
            preference.setSummary(editTextPreference.getText());
        }
    }
}
