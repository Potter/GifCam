package ua.pp.garik.gifcam.fragment;


import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmResults;
import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.api.ApiHelper;
import ua.pp.garik.gifcam.entity.User;
import ua.pp.garik.gifcam.utils.RoundedTransformation;
import ua.pp.garik.gifcam.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {
    public static final String MODE_ID = "mode";
    public static final int MODE_MY_PROFILE = 0;
    public static final int MODE_EXTERNAL_PROFILE = 1;
    public static final String USER_ID = "user";
    private OnProfileInformationSelectedListener mListener;
    private ImageView mUserImage;
    private TextView mUsernameTextView;
    private TextView mLocationTextView;
    private TextView mGifsCountTextView;
    private TextView mFollowersCountTextView;
    private TextView mFollowingCountTextView;
    private Button mFollowButton;
    private Button mUnFollowButton;
    private User mUser;
    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(int mode, User user) {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(MODE_ID, mode);
        if (user != null) {
            args.putSerializable(USER_ID, user);
        }
        profileFragment.setArguments(args);
        return profileFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUserImage = (ImageView) view.findViewById(R.id.user_image);
        mUsernameTextView = (TextView) view.findViewById(R.id.username);
        mLocationTextView = (TextView) view.findViewById(R.id.location);
        mGifsCountTextView = (TextView) view.findViewById(R.id.gif_count);
        mFollowersCountTextView = (TextView) view.findViewById(R.id.followers_count);
        mFollowingCountTextView = (TextView) view.findViewById(R.id.following_count);
        mFollowersCountTextView.setOnClickListener(this);
        mFollowingCountTextView.setOnClickListener(this);
        mFollowButton = (Button) view.findViewById(R.id.follow_button);
        mUnFollowButton = (Button) view.findViewById(R.id.unfollow_button);
        mFollowButton.setOnClickListener(this);
        mUnFollowButton.setOnClickListener(this);
        mGifsCountTextView.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        int mode = args.getInt(MODE_ID, 5);
        switch (mode) {
            case MODE_MY_PROFILE:
                setMyProfileInformation();
                break;
            case MODE_EXTERNAL_PROFILE:
                User user = (User) args.getSerializable(USER_ID);
                mUser = user;
                Realm realm = Realm.getInstance(getActivity());
                RealmResults<User> realmResults = realm.where(User.class)
                    .equalTo("id", user.getId()).findAll();
                if (realmResults.size() > 0) {
                    mFollowButton.setVisibility(View.GONE);
                    mUnFollowButton.setVisibility(View.VISIBLE);
                } else {
                    mUnFollowButton.setVisibility(View.GONE);
                    mFollowButton.setVisibility(View.VISIBLE);
                }
                setUserInformationToLayout(user);
                break;
        }

    }

    public void setMyProfileInformation() {
        Realm realm = Realm.getInstance(getActivity());
        RealmResults<User> results = realm.where(User.class)
            .equalTo("mode", User.PERSONAL_MODE)
            .findAll();
        User user = results.first();
        mUser = user;
        setUserInformationToLayout(user);
    }

    public void setUserInformationToLayout(User user) {
        if (user != null) {
//            Picasso.with(getActivity()).load(Utils.BASE_URL + user.getPhoto()).into(mUserImage);
            Picasso.with(getActivity()).load(Utils.BASE_URL + user.getPhoto()).transform(new RoundedTransformation(100, 8)).resize(200, 200).centerCrop().into(mUserImage);

            mUsernameTextView.setText(user.getFirstName() + " " + user.getLastName());
            mFollowersCountTextView.setText(user.getFollowersCount() + "\nFollowers");
            mFollowingCountTextView.setText(user.getFollowingCount() + "\nFollowing");
            mGifsCountTextView.setText(user.getVideosCount() + "\nGifs");

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.follow_button:
                (new FollowUserTask()).execute(mUser, getActivity());
                break;
            case R.id.unfollow_button:
                (new UnFollowUserTask()).execute(mUser, getActivity());
                break;
            case R.id.followers_count:
                mListener.onUserFollowersSelected(mUser);
                break;
            case R.id.following_count:
                mListener.onUserFollowingSelected(mUser);
                break;
            case R.id.gif_count:
                mListener.onUserGifsSelected(mUser);
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnProfileInformationSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement a OnProfileInformationSelectedListener interface");
        }
    }

    private User getMyUser(){
        Realm realm = Realm.getInstance(getActivity());
        RealmResults<User> users = realm.where(User.class).equalTo("mode", User.PERSONAL_MODE)
            .findAll();
        if (users.size() > 0) {
            return users.first();
        }
        return null;
    }

    public interface OnProfileInformationSelectedListener{
        public void onUserFollowersSelected(User user);

        public void onUserFollowingSelected(User user);

        public void onUserGifsSelected(User user);
    }

    private class FollowUserTask extends AsyncTask<Object, Void, User> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mFollowButton.setEnabled(false);
        }

        @Override
        protected User doInBackground(Object... params) {
            User user = (User) params[0];
            user.setMode(User.EXTERNAL_MODE);
            Activity context = (Activity) params[1];
            (new ApiHelper(context)).getService().followUser(user.getId());
            user.setFollowersCount(user.getFollowersCount() + 1);
            User myUser = getMyUser();
            Realm realm = Realm.getInstance(context);
            realm.beginTransaction();
            realm.copyToRealm(user);
            myUser.setFollowingCount(myUser.getFollowingCount() + 1);
            realm.commitTransaction();

            return user;
        }

        @Override
        protected void onPostExecute(User user) {
            super.onPostExecute(user);
            mUser = user;
            mFollowButton.setVisibility(View.GONE);
            mUnFollowButton.setVisibility(View.VISIBLE);
            mFollowButton.setEnabled(true);
            mFollowersCountTextView.setText(user.getFollowersCount() + "\nFollowers");
        }
    }

    private class UnFollowUserTask extends AsyncTask<Object, Void, User> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mUnFollowButton.setEnabled(false);
        }

        @Override
        protected User doInBackground(Object... params) {
            User user = (User) params[0];
            Activity context = (Activity) params[1];
            (new ApiHelper(context)).getService().unFollowUser(user.getId());
            user.setFollowersCount(user.getFollowersCount() - 1);
            Realm realm = Realm.getInstance(context);
            RealmResults<User> results = realm.where(User.class)
                .equalTo("id", user.getId()).findAll();
            User myUser = getMyUser();
            realm.beginTransaction();
            results.clear();
            myUser.setFollowingCount(myUser.getFollowingCount() - 1);
            realm.commitTransaction();
            return user;
        }

        @Override
        protected void onPostExecute(User user) {
            super.onPostExecute(user);
            mUser = user;
            mUnFollowButton.setVisibility(View.GONE);
            mFollowButton.setVisibility(View.VISIBLE);
            mUnFollowButton.setEnabled(true);
            mFollowersCountTextView.setText(user.getFollowersCount() + "\nFollowers");
        }
    }
}
