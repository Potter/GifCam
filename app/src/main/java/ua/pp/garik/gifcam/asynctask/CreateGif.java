package ua.pp.garik.gifcam.asynctask;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Button;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.activity.MainActivity;
import ua.pp.garik.gifcam.entity.Video;
import ua.pp.garik.gifcam.utils.GifEncoder;
import ua.pp.garik.gifcam.utils.Utils;

/**
 * Created by Gary on 14.03.2015.
 */

public class CreateGif extends AsyncTask<Object, Integer, String[]> {
    private Context context;
    private int framerate;
    private int frameDelay = 1000;
    private boolean upload = false;
    private boolean kill_source = true;
    private Video video;


    public CreateGif(Context contextin) {
        context = contextin;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        framerate = preferences.getInt("framerate", 2);
        frameDelay = Math.round(1000 / framerate);


    }

    @Override
    protected void onPostExecute(String[] strings) {
        super.onPostExecute(strings);
        Button btn_save = (Button)((Activity)this.context).findViewById(R.id.btn_save);
        if (btn_save != null) {
            btn_save.setText("Saved");
        }

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification notification = new Notification.Builder(context)
            .setContentTitle("Job complete")
            .setContentText("GifCam - creating GIF")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pIntent)
            .setAutoCancel(true)
            .build();


        NotificationManager notificationManager =
            (NotificationManager) context.getSystemService(Activity.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification);


        Utils utils = new Utils();
        File outputFolder = utils.prepareOutputDirectory();
        String extension = "";
        if (this instanceof CreateGif) {
            extension = "gif";
        } else {
            extension = "mp4";
        }

        if (outputFolder != null) {
            File mediaStorageDir = utils.getMediaStorageDir();
            if (mediaStorageDir != null) {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                File inputFile = new File(mediaStorageDir.getPath() + File.separator + "movie" + "." + extension);
                //костиль, ага
                while (!inputFile.exists()) {
                }
                File outputFile = new File(outputFolder.getPath() + File.separator + "movie_" + timeStamp + "." + extension);
                inputFile.renameTo(outputFile);
                while (!outputFile.exists()) {
                }

                if (upload) {
                    utils.saveResult(context, outputFile, kill_source, video, framerate);
                } else {
                    if (kill_source) {
                        utils.killSources(mediaStorageDir);
                    }
                }

            }
        }


    }


    protected void onProgressUpdate(Integer... progress) {
        Notification notification = new Notification.Builder(context)
            .setContentTitle("Photo " + progress[0])
            .setProgress(progress[1], progress[0], false)
            .setContentText("GifCam - creating GIF")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setAutoCancel(true)
            .build();


        NotificationManager notificationManager =
            (NotificationManager) context.getSystemService(Activity.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }


    @Override
    protected String[] doInBackground(Object... params) {
        File mediaStorageDir = (File) params[0];
        video = (Video) params[1];
        kill_source = (boolean) params[2];
        upload = (boolean) params[3];

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        GifEncoder encoder = new GifEncoder();
        encoder.start(bos);

        if (mediaStorageDir.isDirectory()) {
            String[] children = mediaStorageDir.list();
            int filesCount = mediaStorageDir.list().length;

            int count = 0;
            for (String aChildren : children) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(mediaStorageDir + "/" + aChildren, options);
                encoder.setDelay(frameDelay);
                encoder.addFrame(bitmap);
                publishProgress(count++, filesCount);
//                new File(mediaStorageDir, aChildren).delete();
            }
        }

        encoder.finish();


        bos.toByteArray();
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), Utils.FOLDER_NAME) + "/movie.gif");
            bos.writeTo(outputStream);
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


}