package ua.pp.garik.gifcam.activity;

import com.facebook.AppEventsLogger;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import android.util.Log;

import java.io.File;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.entity.User;
import ua.pp.garik.gifcam.fragment.CameraFragment;
import ua.pp.garik.gifcam.fragment.FeedFragment;
import ua.pp.garik.gifcam.fragment.NavigationDrawerFragment;
import ua.pp.garik.gifcam.fragment.ProfileFragment;
import ua.pp.garik.gifcam.fragment.SaveFragment;
import ua.pp.garik.gifcam.fragment.SettingsFragment;
import ua.pp.garik.gifcam.fragment.UsersFragment;
import ua.pp.garik.gifcam.fragment.WelcomeFragment;
import ua.pp.garik.gifcam.utils.Utils;

public class MainActivity extends ActionBarActivity implements
    NavigationDrawerFragment.OnNavigationDrawerInteractionListener,
    FeedFragment.OnPostSelectedListener, CameraFragment.OnFragmentInteractionListener,
    UsersFragment.OnUserSelectedListener, ProfileFragment.OnProfileInformationSelectedListener {
    private static final int FEED_ID = 0;
    private static final int MY_GIFS_ID = 1;
    private static final int FAVOURITE_ID = 2;
    private static final int PROFILE_ID = 3;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private User mUser;
    private ConnectivityManager connectivityManager;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address = "00:00:00:00:00:00";
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_ENABLE_BT = 3;
    private NavigationDrawerFragment navigationDrawerFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                ListView menu = (ListView) findViewById(R.id.menu);
                menu.getLayoutParams().height = (menu.findViewById(R.id.menu_item).getHeight() + 1) * menu.getCount();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                ListView menu = (ListView) findViewById(R.id.menu);
                menu.getLayoutParams().height = (menu.findViewById(R.id.menu_item).getHeight() + 1) * menu.getCount();
            }

        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBTState();
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onDrawerListItemClicked(int id) {
        NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
        if (ni != null) {

            switch (id) {
                case FEED_ID:
                    setContentFragment(FeedFragment.newInstance(FeedFragment.MODE_GLOBAL, null));
                    break;
                case MY_GIFS_ID:
                    setContentFragment(FeedFragment.newInstance(FeedFragment.MODE_MY, null));
                    break;
                case FAVOURITE_ID:
                    setContentFragment(FeedFragment.newInstance(FeedFragment.MODE_FAVOURITE, null));
                    break;
                case PROFILE_ID:
                    setContentFragment(ProfileFragment.newInstance(ProfileFragment.MODE_MY_PROFILE, null));
                    break;
            }
        } else {
            Toast.makeText(this, "No internet", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDrawerButtonClicked(int buttonId) {

        switch (buttonId) {
            case R.id.settings:
                setContentFragment(new SettingsFragment());
                break;
            case R.id.create_new:
                setContentFragment(new CameraFragment());
                break;
        }
    }

    @Override
    public void onAttatchToActivity(NavigationDrawerFragment fragment) {
        navigationDrawerFragment = fragment;
    }


    private void setContentFragment(Fragment fragment) {

        if (fragment instanceof CameraFragment) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }


        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.animator.slide_down_anim, R.animator.slide_up_anim);
        transaction.replace(R.id.content, fragment);
        transaction.commit();
        mDrawerLayout.closeDrawer(Gravity.START);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);

    }

    @Override
    public void onPostAuthorSelected(User user) {
        ProfileFragment fragment =
            ProfileFragment.newInstance(ProfileFragment.MODE_EXTERNAL_PROFILE, user);
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.animator.slide_down_anim, R.animator.slide_up_anim);
        transaction.replace(R.id.content, fragment);
        transaction.commit();
        mDrawerLayout.closeDrawer(Gravity.START);
    }

    @Override
    public void onFragmentInteraction(String outputformat, File mediaStorageDir, int photoId) {
        SaveFragment saveFragment = SaveFragment.newInstance(outputformat, mediaStorageDir.getAbsolutePath(), photoId);
        setContentFragment(saveFragment);
    }


    @Override
    public void connectBluetooth() {
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
    }

    @Override
    public boolean sendBluetooth(String data) {
        sendData(data);
        return false;
    }

    @Override
    public void onUserSelected(User user) {
        if (getMyUser() != null) {
            if (user.getId() != getMyUser().getId()) {
                setContentFragment(ProfileFragment.newInstance(ProfileFragment.MODE_EXTERNAL_PROFILE, user));
            } else {
                setContentFragment(ProfileFragment.newInstance(ProfileFragment.MODE_MY_PROFILE, user));
            }
        }
    }

    @Override
    public void onUserFollowersSelected(User user) {
        setContentFragment(UsersFragment.newInstance(UsersFragment.MODE_FOLLOWERS, user));
    }

    @Override
    public void onUserFollowingSelected(User user) {
        setContentFragment(UsersFragment.newInstance(UsersFragment.MODE_FOLLOWING, user));
    }

    @Override
    public void onUserGifsSelected(User user) {
        setContentFragment(FeedFragment.newInstance(FeedFragment.MODE_USER, user));
    }

    private User getMyUser() {
        Realm realm = Realm.getInstance(this);
        RealmResults<User> users = realm.where(User.class).equalTo("mode", User.PERSONAL_MODE)
            .findAll();
        if (users.size() > 0) {
            return users.first();
        }
        return null;
    }



    private void checkBTState() {
        // Check for Bluetooth support and then check to make sure it is turned on
        // Emulator doesn't support Bluetooth and will return null
        if(btAdapter==null) {
            errorExit("Fatal Error", "Bluetooth Not supported. Aborting.");
        } else {
            if (btAdapter.isEnabled()) {
                Log.d("GifCam", "...Bluetooth is enabled...");
            } else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(btAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private void errorExit(String title, String message){
        Toast msg = Toast.makeText(getBaseContext(), title + " - " + message, Toast.LENGTH_SHORT);
        msg.show();
        finish();
    }

    private void sendData(String message) {
        byte[] msgBuffer = message.getBytes();
        try {
            outStream.write(msgBuffer);
        } catch (Exception e) {
            String msg = "In onResume() and an exception occurred during write: " + e.getMessage();
            errorExit("Fatal Error", msg);
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("GifCam Bluetooth", "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);

        if (navigationDrawerFragment != null) {
            navigationDrawerFragment.onActivityResult(requestCode, resultCode, data);
        }

        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
           //     When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        Log.d("GifCam Bluetooth", "connectDevice");
        address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
//        text.setText("Device Address: " + address);
        connectToDevice(address);
        // Get the BluetoothDevice object
        BluetoothDevice device = btAdapter.getRemoteDevice(address);
    }

    public void connectToDevice(String adr) {
        super.onResume();
        //enable buttons once connection established.
//        btnOn.setEnabled(true);
//        btnOff.setEnabled(true);
        Toast.makeText(this, "Connected: " + adr, Toast.LENGTH_LONG).show();
        Log.d("GifCam Bluetooth", "connectToDevice " + adr);
        // Set up a pointer to the remote node using it's address.
        BluetoothDevice device = btAdapter.getRemoteDevice(adr);
        // Two things are needed to make a connection:
        //   A MAC address, which we got above.
        //   A Service ID or UUID.  In this case we are using the
        //     UUID for SPP.
        try {
            btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (Exception e) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        btAdapter.cancelDiscovery();
        // Establish the connection.  This will block until it connects.
        try {
            btSocket.connect();
        } catch (Exception e) {
            try {
                btSocket.close();
            } catch (Exception e2) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
            }
        }
        // Create a data stream so we can talk to server.
        try {
            outStream = btSocket.getOutputStream();
        } catch (Exception e) {
            errorExit("Fatal Error", "In onResume() and output stream creation failed:" + e.getMessage() + ".");
        }

        Fragment fragment = getFragmentManager().findFragmentById(R.id.content);
        if (fragment instanceof CameraFragment) {
            ((CameraFragment)fragment).activateBluetooth();
        }
    }

}
