package ua.pp.garik.gifcam.preferences;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.ArrayList;

import ua.pp.garik.gifcam.R;

public class NumberPickerPreference extends DialogPreference {
    private static final int MIN_VALUE = 100;
    private static final int MAX_VALUE = 1000;
    private static final boolean WRAP_SELECTOR_WHEEL = false;
    private final int mMinValue;
    private final int mMaxValue;
    private final boolean mWrapSelectorWheel;
    private int mSelectedValue;
    private NumberPicker mNumberPicker;

    public NumberPickerPreference(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NumberPickerPreference);

        mMinValue = a.getInt(R.styleable.NumberPickerPreference_minValue, NumberPickerPreference.MIN_VALUE);
        mMaxValue = a.getInt(R.styleable.NumberPickerPreference_maxValue, NumberPickerPreference.MAX_VALUE);
        mWrapSelectorWheel = a.getBoolean(R.styleable.NumberPickerPreference_setWrapSelectorWheel,
            NumberPickerPreference.WRAP_SELECTOR_WHEEL);

        a.recycle();
        setDialogLayoutResource(R.layout.number_preference);
    }

    @Override
    protected void onSetInitialValue(final boolean restoreValue, final Object defaultValue) {
        mSelectedValue = restoreValue ? this.getPersistedInt(0) : (Integer) defaultValue;
        this.updateSummary();
    }

    @Override
    protected Object onGetDefaultValue(final TypedArray a, final int index) {
        return a.getInteger(index, 0);
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        mNumberPicker = (NumberPicker) view.findViewById(R.id.picker);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        ((TextView) view.findViewById(android.R.id.title)).setTextColor
            (getContext().getResources().getColor(R.color.preference_text));
        ((TextView) view.findViewById(android.R.id.summary)).setTextColor
            (getContext().getResources().getColor(R.color.preference_text));
    }

    @Override
    protected void onPrepareDialogBuilder(final Builder builder) {
        super.onPrepareDialogBuilder(builder);
        mNumberPicker.setMaxValue(mMaxValue);
        mNumberPicker.setMinValue(mMinValue);
        mNumberPicker.setValue(mSelectedValue);
        ArrayList<String> numbers = new ArrayList<>();
        for (int i = mMinValue; i <= mMaxValue; i++) {
            numbers.add(Integer.toString(i));
        }
        mNumberPicker.setWrapSelectorWheel(mWrapSelectorWheel);
        String[] stringNumbers = new String[numbers.size()];
        mNumberPicker.setDisplayedValues(numbers.toArray(stringNumbers));

    }

    @Override
    protected void onDialogClosed(final boolean positiveResult) {
        if (positiveResult && this.shouldPersist()) {
            mSelectedValue = mNumberPicker.getValue();
            this.persistInt(mSelectedValue);
            this.updateSummary();
        }
    }

    private void updateSummary() {
        super.setSummary(super.getTitle() + " " + mSelectedValue);
    }
}