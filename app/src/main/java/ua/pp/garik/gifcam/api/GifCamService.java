package ua.pp.garik.gifcam.api;

import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import ua.pp.garik.gifcam.entity.User;
import ua.pp.garik.gifcam.utils.FeedContainer;
import ua.pp.garik.gifcam.utils.FollowersContainer;
import ua.pp.garik.gifcam.utils.FollowingContainer;
import ua.pp.garik.gifcam.utils.PostResult;
import ua.pp.garik.gifcam.utils.RegistrationContainer;
import ua.pp.garik.gifcam.utils.VideoContainer;

/**
 * Created by root on 14.03.15.
 */
public interface GifCamService {
    @FormUrlEncoded
    @POST("/api/user/register")
    RegistrationContainer registerUserByToken(@Field("provider") String provider, @Field("token") String token);

    @POST("/api/user")
    void updateUser(@Body User user);

    @GET("/api/user/{id}")
    User getUser(@Path("id") long id);

    @GET("/api/user/{id}/video")
    FeedContainer getUserVideos(@Path("id") long id);

    @GET("/api/user/{id}/followers")
    FollowersContainer getUserFollowers(@Path("id") long id);

    @GET("/api/user/{id}/following")
    FollowingContainer getUserFollowing(@Path("id") long id);

    @GET("/api/user/{id}/favorites")
    FeedContainer getUserFavorites(@Path("id") long id);
    @POST("/api/user/{id}/follow")
    PostResult followUser(@Path("id") long id);

    @POST("/api/user/{id}/unfollow")
    PostResult unFollowUser(@Path("id") long id);

    @POST("/api/video/{id}/fav")
    VideoContainer favouriteVideo(@Path("id") long id);

    @POST("/api/video/{id}/unfav")
    VideoContainer unFavouriteVideo(@Path("id") long id);

    @GET("/api/video/{id}")
    VideoContainer getVideoContainer(@Path("id") long id);

    @GET("/api/video/feed")
    FeedContainer getVideoFeed(@Query("count") Long count, @Query("offset") Long offset);
}
