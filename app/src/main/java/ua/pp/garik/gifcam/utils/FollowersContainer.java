package ua.pp.garik.gifcam.utils;

import java.util.List;

import ua.pp.garik.gifcam.entity.User;

/**
 * Created by Vova on 03.04.2015.
 */
public class FollowersContainer {
    private List<User> followers;

    public List<User> getFollowers() {
        return followers;
    }

    public void setFollowers(List<User> followers) {
        this.followers = followers;
    }
}
