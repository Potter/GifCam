package ua.pp.garik.gifcam.fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.camera.BaseCameraFragment;
import ua.pp.garik.gifcam.utils.Utils;

public class CameraFragment extends BaseCameraFragment implements View.OnClickListener {
    Timer timer;
    private TextView tv1;
    private Button btnStart, btnPause, btnStop;
    private ProgressBar progressBar;
    private int sessionLength = 10;
    private int sessionInterval = 10;
    private String outputformat;
    private String offsets;
    private String direction;
    private String halfstep;
    private int reverse_after;
    private int stepsCount = 1;
    private int shotsBeforeStep = 1;
    private int shotsBeforeReverse = 150;
    private boolean sessionRunning = false;
    private boolean isBluetooth = false;

    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (sessionRunning) {
                        try {
                            if (safeToTakePicture) {
                                mCamera.takePicture(null, null, mPicture);
                                safeToTakePicture = false;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        tv1.setText("Progress: " + Integer.toString(photoId + 1) + "/" + sessionLength);
                        progressBar.setProgress(photoId + 1);

                        if (photoId % shotsBeforeStep == 0) {
                            if (isBluetooth) { //bluetooth
                                mListener.sendBluetooth(direction + " " + stepsCount + " " + halfstep + " 0");
                            } else { //audio
                                for (int i = 0; i < stepsCount; i++) {
                                    tv1.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (audioManager.isWiredHeadsetOn()) {
                                                toneGenerator.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200); // 200 is duration in ms
                                            }
                                        }
                                    }, i * 300 + 200);
                                }
                            }
                        }

                        photoId++;
                        shotsBeforeReverse--;
                        if (shotsBeforeReverse <= 0) {
                            shotsBeforeReverse = reverse_after;
                            if (direction.equals("cw")) {
                                direction = "ccw";
                            } else {
                                direction = "cw";
                            }
                        }

                        if (photoId >= sessionLength) {
                            sessionRunning = false;
                            stopSession();
                        }
                    }
                }
            });
        }
    };
    private ToneGenerator toneGenerator;
    private AudioManager audioManager;
    private OnFragmentInteractionListener mListener;

    public CameraFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_camera, menu);
        if(isBluetooth) {
            menu.findItem(R.id.action_test).setVisible(true);
        } else {
            menu.findItem(R.id.action_test).setVisible(false);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_create_from_folder) {
            prepareStorageDirectory();
            stopSession();
        }
        if (id == R.id.action_bluetooth) {
            mListener.connectBluetooth();
        }
        if (id == R.id.action_test) {
            mListener.sendBluetooth(direction + " " + reverse_after + " " + halfstep + " 0");
            btnStart.postDelayed(new Runnable() {
                @Override
                public void run() {
                    String reverseDirection = "cw";
                    if (direction.equals("cw")) {
                        reverseDirection = "ccw";
                    }
                    mListener.sendBluetooth(reverseDirection + " " + reverse_after + " " + halfstep + " 0");
                }
            }, 10000);
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        sessionLength = preferences.getInt("session", 50);
        sessionInterval = preferences.getInt("interval", 5);
        outputformat = preferences.getString("outputformat", "gif");
//        steps_count = preferences.getInt("steps_count", 1);

        direction = preferences.getString("direction", "cw");
        halfstep = preferences.getString("halfstep", "0");
        reverse_after = preferences.getInt("reverse_after", 150);
        shotsBeforeReverse = reverse_after;

        offsets =  preferences.getString("offsets", "1:1");
        String[] offsetParams = offsets.split(":", -1);
        if (offsetParams.length == 2) {
            stepsCount = Integer.parseInt(offsetParams[0]);
            shotsBeforeStep = Integer.parseInt(offsetParams[1]);
        }

        btnStart = (Button) view.findViewById(R.id.button_start);
        btnStart.setOnClickListener(this);
        btnPause = (Button) view.findViewById(R.id.button_pause);
        btnPause.setOnClickListener(this);
        btnStop = (Button) view.findViewById(R.id.button_stop);
        btnStop.setOnClickListener(this);
        timer = new Timer();
        tv1 = (TextView) view.findViewById(R.id.tv1);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        progressBar.setMax(sessionLength);

        audioManager = (AudioManager) getActivity().getSystemService(getActivity().AUDIO_SERVICE);
        toneGenerator = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_start: {
                prepareStorageDirectory();
                sessionRunning = true;
                timer = new Timer();
                photoId = 0;
                timer.scheduleAtFixedRate(timerTask, 2000, sessionInterval * 1000);
                btnStart.setVisibility(View.GONE);
                btnPause.setVisibility(View.VISIBLE);
                btnStop.setVisibility(View.VISIBLE);
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
            break;
            case R.id.button_pause: {
                sessionRunning = !sessionRunning;
                togglePause();
            }
            break;
            case R.id.button_stop: {
                stopSession();
            }
            break;
        }
    }

    private void togglePause() {
        if (sessionRunning) {
//            btnPause.setText("Pause");
            btnPause.setBackground(getResources().getDrawable(R.drawable.ic_pause));
        } else {
//            btnPause.setText("Continue");
            btnPause.setBackground(getResources().getDrawable(R.drawable.ic_play));
        }
    }

    private void stopSession() {
        sessionRunning = false;
        timer.cancel();
        timer = null;
        btnPause.setVisibility(View.GONE);
        btnStop.setVisibility(View.GONE);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mListener.onFragmentInteraction(outputformat, mediaStorageDir, photoId);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void activateBluetooth() {
        isBluetooth = true;
        getActivity().invalidateOptionsMenu();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String outputformat, File mediaStorageDir, int photoId);
        void connectBluetooth();
        boolean sendBluetooth(String data);
    }
}
