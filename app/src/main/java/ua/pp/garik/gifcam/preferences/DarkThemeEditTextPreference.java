package ua.pp.garik.gifcam.preferences;

import android.content.Context;
import android.preference.EditTextPreference;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import ua.pp.garik.gifcam.R;

public class DarkThemeEditTextPreference extends EditTextPreference {
    public DarkThemeEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onBindView(@NonNull View view) {
        super.onBindView(view);
        ((TextView) view.findViewById(android.R.id.title)).setTextColor
            (getContext().getResources().getColor(R.color.preference_text));
        ((TextView) view.findViewById(android.R.id.summary)).setTextColor
            (getContext().getResources().getColor(R.color.preference_text));
    }
}
