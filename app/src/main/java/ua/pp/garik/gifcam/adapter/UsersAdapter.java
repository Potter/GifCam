package ua.pp.garik.gifcam.adapter;

import com.squareup.picasso.Picasso;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.entity.User;
import ua.pp.garik.gifcam.utils.RoundedTransformation;
import ua.pp.garik.gifcam.utils.Utils;

/**
 * Created by Vova on 03.04.2015.
 */
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {
    private List<User> mUsers;
    private OnUserRowSelectedListener mListener;

    public UsersAdapter(OnUserRowSelectedListener listener) {
        mUsers = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.user_item_layout, parent, false);
        return new ViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = mUsers.get(position);
//        Picasso.with(holder.mUserLogoImageView.getContext()).load(Utils.BASE_URL + user.getPhoto()).into(holder.mUserLogoImageView);
        Picasso.with(holder.mUserLogoImageView.getContext()).load(Utils.BASE_URL + user.getPhoto()).transform(new RoundedTransformation(50, 4)).resize(100, 100).centerCrop().into(holder.mUserLogoImageView);
        holder.mUsernameTextView.setText(user.getFirstName() + " " + user.getLastName());
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public void addData(List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            mUsers.add(user);
            notifyItemInserted(i);
        }
    }

    public User getItem(int position) {
        return mUsers.get(position);
    }

    public interface OnUserRowSelectedListener {
        public void onUserItemSelected(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView mUserLogoImageView;
        private TextView mUsernameTextView;
        private OnUserRowSelectedListener mListener;

        public ViewHolder(View itemView, OnUserRowSelectedListener listener) {
            super(itemView);
            mUserLogoImageView = (ImageView) itemView.findViewById(R.id.user_logo);
            mUsernameTextView = (TextView) itemView.findViewById(R.id.username);
            mListener = listener;
            mUsernameTextView.setOnClickListener(this);
            mUserLogoImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onUserItemSelected(getPosition());
        }
    }
}
