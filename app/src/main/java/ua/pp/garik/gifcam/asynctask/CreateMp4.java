package ua.pp.garik.gifcam.asynctask;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.activity.MainActivity;
import ua.pp.garik.gifcam.entity.Video;
import ua.pp.garik.gifcam.utils.Utils;

/**
 * Created by Gary on 14.03.2015.
 */

public class CreateMp4 extends AsyncTask<Object, Integer, String[]> {
    FFmpeg ffmpeg;
    private Context context;
    private int framerate;
    private boolean upload = false;
    private boolean kill_source = true;
    private Video video;

    public CreateMp4(Context contextin) {
        context = contextin;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        framerate = preferences.getInt("framerate", 2);
    }

    @Override
    protected void onPostExecute(String[] strings) {
        super.onPostExecute(strings);
        showFinalNotification("");

        Utils utils = new Utils();
        File outputFolder = utils.prepareOutputDirectory();
        String extension = "";
        if (this instanceof CreateMp4) {
            extension = "mp4";
        } else {
            extension = "gif";
        }


        if (outputFolder != null) {
            File mediaStorageDir = utils.getMediaStorageDir();
            if (mediaStorageDir != null) {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                File inputFile = new File(mediaStorageDir.getPath() + File.separator + "movie" + "." + extension);
                //костиль, ага
                while (!inputFile.exists()) {
                }
                File outputFile = new File(outputFolder.getPath() + File.separator + "movie_" + timeStamp + "." + extension);
                inputFile.renameTo(outputFile);
                while (!outputFile.exists()) {
                }

                if (upload) {
                    utils.saveResult(context, outputFile, kill_source, video, framerate);
                }

                //file, shotsCount,
            }
        }


    }

    protected void onProgressUpdate(Integer... progress) {
        Log.d("tag", "progress" + progress[0]);
        showNotification("progress" + progress[0]);
    }

    @Override
    protected String[] doInBackground(Object... params) {
        File mediaStorageDir = (File) params[0];
        video = (Video) params[1];
        kill_source = (boolean) params[2];
        upload = (boolean) params[3];


//        String command = "scale=iw:ih -r " + framerate + " -i /sdcard/Pictures/" + Utils.FOLDER_NAME + "/img%d.jpg /sdcard/Pictures/" + Utils.FOLDER_NAME + "/movi2.mp4";
        String command = "-r " + framerate + " -i /sdcard/Pictures/" + Utils.FOLDER_NAME + "/img%d.jpg /sdcard/Pictures/" + Utils.FOLDER_NAME + "/movie.mp4";
        loadFFMpegBinary();
        execFFmpegBinary(command);
        return null;
    }


    private void loadFFMpegBinary() {
        try {
            ffmpeg = FFmpeg.getInstance(context);
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        }
    }

    private void execFFmpegBinary(final String command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    showNotification("FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {
                    showNotification("SUCCESS with output : " + s);
                }

                @Override
                public void onProgress(String s) {
                    showNotification("progress : " + s);
                }

                @Override
                public void onStart() {
                    showNotification("Processing...");
                }

                @Override
                public void onFinish() {
                    Log.d("tag", "Finished command : ffmpeg " + command);
                    showFinalNotification("");
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }

    private void showUnsupportedExceptionDialog() {
        Log.d("tag", "Mp4 - unsupported on your device");
        Toast.makeText(context, "Mp4 - unsupported on your device", Toast.LENGTH_LONG).show();
    }

    private void showNotification(String string) {
        Notification notification = new Notification.Builder(context)
            .setContentTitle(string)
            .setContentText("GifCam - creating MP4")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setAutoCancel(true)
            .build();


        NotificationManager notificationManager =
            (NotificationManager) context.getSystemService(Activity.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

    private void showFinalNotification(String string) {
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification notification = new Notification.Builder(context)
            .setContentTitle("Job complete")
            .setContentText("GifCam - creating MP4")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pIntent)
            .setAutoCancel(true)
            .build();


        NotificationManager notificationManager =
            (NotificationManager) context.getSystemService(Activity.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification);
    }


}