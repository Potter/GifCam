package ua.pp.garik.gifcam.asynctask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

import io.realm.Realm;
import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.activity.MainActivity;
import ua.pp.garik.gifcam.entity.Video;
import ua.pp.garik.gifcam.utils.Utils;

public class AsyncHttpPostTask extends AsyncTask<Object, Void, String> {

    private static final String TAG = AsyncHttpPostTask.class.getSimpleName();
    private String server;
    private Context context;
    private boolean kill_source = false;
    private File mediaStorageDir;
    private Video video;
    private int framerate;

    public AsyncHttpPostTask(Context context, final String server) {
        this.server = server;
        this.context = context;
    }


    @Override
    protected void onPostExecute(String string) {
//        Log.d("JSON", string);
        Toast.makeText(context, "Upload complete", Toast.LENGTH_LONG).show();

        Button btn_save = (Button)((Activity)this.context).findViewById(R.id.btn_save);
        if (btn_save != null) {
            btn_save.setText("Upload complete");
        }

        if (kill_source) {
            Utils utils = new Utils();
            utils.killSources(mediaStorageDir);
        }

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification notification = new Notification.Builder(context)
            .setContentTitle("Upload complete")
            .setContentText("GifCam - uploading to server")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pIntent)
            .setAutoCancel(true)
            .build();


        NotificationManager notificationManager =
            (NotificationManager) context.getSystemService(Activity.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification);


    }

    @Override
    protected  void onPreExecute()
    {
        Button btn = (Button)((Activity)this.context).findViewById(R.id.btn_save);
        if (btn != null) {
            btn.setText("Uploading...");
        }
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification notification = new Notification.Builder(context)
                .setContentTitle("Uploading...")
                .setContentText("GifCam - uploading to server")
            .setSmallIcon(R.mipmap.ic_launcher)
                .setProgress(100, 0, true)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .build();


        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Activity.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification);


    }

    @Override
    protected String doInBackground(Object... params) {
        Log.d(TAG, "doInBackground");
        try {



            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(this.server);
            FileBody fileVideoBody = new FileBody((File) params[0]);
            FileBody fileThumbBody = new FileBody((File) params[1]);
            kill_source = (boolean) params[2];
            mediaStorageDir = (File) params[3];
            video = (Video) params[4];
            framerate = (int) params[5];
//            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences preferences = ((Activity)context).getPreferences(Context.MODE_PRIVATE);
            String apikey = preferences.getString("apikey", "");

            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("video", fileVideoBody);
            reqEntity.addPart("image", fileThumbBody);
            reqEntity.addPart("title", new StringBody(video.getTitle()));
            reqEntity.addPart("description", new StringBody(video.getDescription()));
            reqEntity.addPart("latitude", new StringBody(video.getLatitude()));
            reqEntity.addPart("longitude", new StringBody(video.getLongitude()));
            reqEntity.addPart("shotsCount", new StringBody(String.valueOf(video.getShotsCount())));
            httpPost.setEntity(reqEntity);

            httpPost.setHeader("apikey", apikey);
            HttpResponse response = null;

            response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == 200) {
                //ok
                HttpEntity resEntity = response.getEntity();
                String responseStr = EntityUtils.toString(resEntity).trim();
                Log.d("RESPONSE", responseStr);

                JSONObject jObject = new JSONObject(responseStr);
                long id = jObject.getJSONObject("video").getInt("id");
                String path = jObject.getJSONObject("video").getString("url");
                String thumb = jObject.getJSONObject("video").getString("thumb");
                long created = jObject.getJSONObject("video").getLong("created");

                video.setId(id);
                video.setCreated(created);
                video.setThumb(thumb);
                video.setPath(path);
                video.setDuration((int) (video.getShotsCount() / framerate));
                video.setMode(Video.MODE_MY);

                Realm realm = Realm.getInstance(context);
                realm.beginTransaction();
//                realm.copyToRealmOrUpdate(video);
                Video realmVideo = realm.copyToRealm(video);
                realm.commitTransaction();


                return "Uploaded successfully";
            } else {
                return "Something went wrong";
            }


//            return responseStr;
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }
}