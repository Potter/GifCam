package ua.pp.garik.gifcam.fragment;


import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmResults;
import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.api.ApiHelper;
import ua.pp.garik.gifcam.api.GifCamService;
import ua.pp.garik.gifcam.entity.User;
import ua.pp.garik.gifcam.utils.RegistrationContainer;
import ua.pp.garik.gifcam.utils.RoundedTransformation;
import ua.pp.garik.gifcam.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private ListView mMenuList;
    private OnNavigationDrawerInteractionListener listener;
    private UiLifecycleHelper mUiLifecycleHelper;
    private LoginButton authButton;
    private ImageView profileImageView;
    private TextView usernameView;
    private String FACEBOOK_LOGINED_KEY = "facebookLogined";
    private boolean facebookLogined = false;

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState sessionState, Exception e) {
            onSessionStateChange(session, sessionState, e);
        }
    };

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listener.onDrawerListItemClicked(position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnNavigationDrawerInteractionListener) activity;
            listener.onAttatchToActivity(this);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            facebookLogined = savedInstanceState.getBoolean(FACEBOOK_LOGINED_KEY, facebookLogined);
        }
        mUiLifecycleHelper = new UiLifecycleHelper(getActivity(), callback);
        mUiLifecycleHelper.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mUiLifecycleHelper.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUiLifecycleHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(FACEBOOK_LOGINED_KEY, facebookLogined);

        super.onSaveInstanceState(outState);
        mUiLifecycleHelper.onSaveInstanceState(outState);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mUiLifecycleHelper.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        profileImageView = (ImageView) view.findViewById(R.id.user_image);
        usernameView = (TextView) view.findViewById(R.id.username);
        mMenuList = (ListView) view.findViewById(R.id.menu);
        Button createNewButton = (Button) view.findViewById(R.id.create_new);
        Button settingsButton = (Button) view.findViewById(R.id.settings);
        createNewButton.setOnClickListener(this);
        settingsButton.setOnClickListener(this);
        mMenuList.setOnItemClickListener(this);
        authButton = (LoginButton) view.findViewById(R.id.authButton);
        authButton.setReadPermissions("public_profile", "email", "user_birthday");
        authButton.setFragment(this);


    }

    @Override
    public void onClick(View v) {
        listener.onDrawerButtonClicked(v.getId());
    }

    @Override
    public void onStart() {
        super.onStart();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
            R.layout.nav_drawer_list_item_layout, R.id.item_text
            , getResources().getStringArray(R.array.nav_menu_list));
        mMenuList.setAdapter(adapter);
        ListView menu = (ListView) getActivity().findViewById(R.id.menu);
        menu.getLayoutParams().height = 88;
        mMenuList.getLayoutParams().height = mMenuList.getCount() * (menu.getLayoutParams().height + 1);

        Realm realm = Realm.getInstance(getActivity());
        RealmResults<User> users = realm.where(User.class).equalTo("mode", User.PERSONAL_MODE).findAll();
        if (users.size() > 0) {
            User user = users.first();
            if (user != null) {
                authButton.setVisibility(View.GONE);
                usernameView.setText(user.getFirstName() + " " + user.getLastName());
//            Picasso.with(getActivity()).load(Utils.BASE_URL + user.getPhoto()).into(profileImageView);
                Picasso.with(getActivity())
                    .load(Utils.BASE_URL + user.getPhoto()).transform(new RoundedTransformation(50, 4))
                    .resize(100, 100)
                    .centerCrop().into(profileImageView);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mUiLifecycleHelper.onPause();
    }

    private void onSessionStateChange(Session session, SessionState sessionState, Exception e) {
        if (sessionState.isOpened() && !facebookLogined) {

            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = cm.getActiveNetworkInfo();
            if (ni != null) {
                (new FacebookLoginAsyncTask(getActivity())).execute(session.getAccessToken());
            } else {
                Toast.makeText(getActivity(), "No internet", Toast.LENGTH_LONG).show();
            }


        }
    }

    public interface OnNavigationDrawerInteractionListener {
        public void onDrawerListItemClicked(int id);

        public void onDrawerButtonClicked(int buttonId);

        public void onAttatchToActivity(NavigationDrawerFragment fragment);

    }

    private class FacebookLoginAsyncTask extends AsyncTask<String, Void, User> {
        private Context mContext;

        private FacebookLoginAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected User doInBackground(String... params) {
            ApiHelper helper = new ApiHelper(getActivity());
            GifCamService service = helper.getLoginService();
            RegistrationContainer container = service.registerUserByToken("facebook", params[0]);
            User user = container.getUser();
            user.setMode(User.PERSONAL_MODE);

            Realm realm = Realm.getInstance(getActivity());
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();

//            try {
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            Log.d("GARR", "do in background");

            SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("apikey", container.getApikey());
            editor.apply();
            return user;
        }

        @Override
        protected void onPostExecute(User user) {
            super.onPostExecute(user);
            authButton.setVisibility(View.GONE);
            usernameView.setText(user.getFirstName() + " " + user.getLastName());
            Picasso.with(getActivity())
                .load(Utils.BASE_URL + user.getPhoto()).transform(new RoundedTransformation(50, 4))
                .resize(100, 100)
                .centerCrop().into(profileImageView);
            facebookLogined = true;
        }
    }
}
