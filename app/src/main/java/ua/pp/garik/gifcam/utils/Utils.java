package ua.pp.garik.gifcam.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Button;

import java.io.File;

import ua.pp.garik.gifcam.asynctask.AsyncHttpPostTask;
import ua.pp.garik.gifcam.entity.Video;

/**
 * Created by Gary on 15.03.2015.
 */
public class Utils {
    public final static String FOLDER_NAME = "GifCam";
    public final static String FOLDER_VIDEO_NAME = "GifCam_output";
    public final static String BASE_URL = "http://gifcam.uw-t.com";

    protected File mediaStorageDir;

    public File prepareOutputDirectory() {
        mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES), Utils.FOLDER_VIDEO_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(Utils.FOLDER_VIDEO_NAME, "failed to create directory");
                return null;
            }
        }

        return mediaStorageDir;
    }

    public File getMediaStorageDir() {
        mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES), Utils.FOLDER_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(Utils.FOLDER_NAME, "failed to create directory");
                return null;
            }
        }

        return mediaStorageDir;
    }

    public void sendVideo() {
//        Log.d("SEND", "FILE");
//
//        File mediaStorageDir = this.getMediaStorageDir();
//        final File inputFile = new File(mediaStorageDir.getPath() + File.separator + "img1.jpg");
//
//        final String url = "http://gifcam.uw-t.com/api/video";
//        new AsyncHttpPostTask(url).execute(inputFile);


    }

    public void saveResult(Context context, File resultFile, boolean kill_source, Video video, int framerate) {
        final String url = "http://gifcam.uw-t.com/api/video";
        File mediaStorageDir = this.getMediaStorageDir();
        final File thumbFile = new File(mediaStorageDir.getPath() + File.separator + "img1.jpg");
        new AsyncHttpPostTask(context, url).execute(resultFile, thumbFile, kill_source, mediaStorageDir, video, framerate);
    }

    public void killSources(File mediaStorageDir) {
        if (mediaStorageDir.isDirectory()) {
            String[] children = mediaStorageDir.list();
            for (int i = 0; i < children.length; i++) {
                new File(mediaStorageDir, children[i]).delete();
            }
        }
    }


}
