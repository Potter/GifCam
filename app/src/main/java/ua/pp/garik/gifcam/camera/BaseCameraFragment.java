package ua.pp.garik.gifcam.camera;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.FrameLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import ua.pp.garik.gifcam.R;
import ua.pp.garik.gifcam.utils.Utils;

public class BaseCameraFragment extends Fragment {
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    protected boolean safeToTakePicture = true;
    protected String LOG_TAG = "GARY__" + this.getClass().getSimpleName();
    protected Camera.PictureCallback mPicture = new Camera.PictureCallback() {


        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null) {
                Log.d(LOG_TAG, "Error creating media file, check storage permissions: ");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                Log.d(LOG_TAG, "ok da : " + pictureFile.getAbsolutePath());
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d(LOG_TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(LOG_TAG, "Error accessing file: " + e.getMessage());
            }


            mCamera.startPreview();
            safeToTakePicture = true;

            /*FrameLayout preview = (FrameLayout) getActivity().findViewById(R.id.camera_preview);



            preview.removeAllViews();
            preview.addView(mPreview);*/

        }
    };
    protected Camera mCamera;
    protected CameraPreview mPreview;
    protected int photoId = 0;
    protected File mediaStorageDir;
    private MediaRecorder mMediaRecorder;

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {

        }
        return c;
    }

    private boolean checkCameraHardware(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    protected boolean prepareStorageDirectory() {
        mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES), Utils.FOLDER_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(Utils.FOLDER_NAME, "failed to create directory");
                return false;
            }
        }

        return true;
    }


    /** Create a File for saving an image or video */
    private File getOutputMediaFile(int type) {
        if (mediaStorageDir == null) {
            prepareStorageDirectory();
        }

        // Create a media file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "img" + photoId + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "vid" + photoId + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    /** Create a file Uri for saving an image or video */
    private Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mCamera == null) {
            mCamera = getCameraInstance();
            Camera.Parameters parameters = mCamera.getParameters();

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String pictureSize = preferences.getString("pictureSize", "1280x720");
            String[] sizez = pictureSize.split("x", -1);
            if (sizez.length == 2) {

                boolean order_desc = true;
                int prev_width = 0;
                for (Iterator<Camera.Size> i = parameters.getSupportedPictureSizes().iterator(); i.hasNext(); ) {
                    Camera.Size item = i.next();
                    if (prev_width == 0) {
                        prev_width = item.width;
                        continue;
                    }
                    if (item.width < prev_width) {
                        order_desc = true;
                        break;
                    }
                    if (item.width > prev_width) {
                        order_desc = false;
                        break;
                    }
                }


                for (Iterator<Camera.Size> i = parameters.getSupportedPictureSizes().iterator(); i.hasNext(); ) {
                    Camera.Size item = i.next();
                    if (!order_desc && item.height >= Integer.parseInt(sizez[1]) || order_desc && item.height <= Integer.parseInt(sizez[1])) {
                        parameters.setPictureSize(item.width, item.height);
                        break;
                    }
                }
            } else {
                //parameters.setPictureSize(640, 360);
            }
//            parameters.setColorEffect("negative");


            List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
            Camera.Size size = sizes.get(0);
            parameters.setPreviewSize(size.width, size.height);

            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
            parameters.setJpegQuality(100);


            try {
                mCamera.setParameters(parameters);
            } catch (Exception e) {
                Camera.Parameters anotherParameters = mCamera.getParameters();
                anotherParameters.setJpegQuality(parameters.getJpegQuality());
                anotherParameters.setPreviewSize(size.width, size.height);
                Camera.Size sz = parameters.getPictureSize();
                anotherParameters.setPictureSize(sz.width, sz.height);
                mCamera.setParameters(anotherParameters);
            }
            mPreview = new CameraPreview(getActivity(), mCamera);
            FrameLayout preview = (FrameLayout) getActivity().findViewById(R.id.camera_preview);
            preview.addView(mPreview);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }
}
