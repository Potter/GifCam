package ua.pp.garik.gifcam.api;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import io.realm.RealmObject;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Vova on 22.03.2015.
 */
public class ApiHelper {
    private Activity mContext;

    public ApiHelper(Activity context) {
        mContext = context;
    }

    public GifCamService getLoginService() {
        Gson gson = new GsonBuilder()
            .setExclusionStrategies(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    return f.getDeclaringClass().equals(RealmObject.class);
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }
            })
            .create();


        RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint("http://gifcam.uw-t.com")
            .setConverter(new GsonConverter(gson))
            .build();

        return restAdapter.create(GifCamService.class);
    }

    public GifCamService getService() {
        Gson gson = new GsonBuilder()
            .setExclusionStrategies(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    return f.getDeclaringClass().equals(RealmObject.class);
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }
            })
            .create();

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                SharedPreferences preferences = mContext.getPreferences(Context.MODE_PRIVATE);
                String apiKey = preferences.getString("apikey", "default");
                request.addHeader("apikey", apiKey);
            }
        };
        RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint("http://gifcam.uw-t.com")
            .setRequestInterceptor(requestInterceptor)
            .setConverter(new GsonConverter(gson))
            .build();
        return restAdapter.create(GifCamService.class);
    }
}