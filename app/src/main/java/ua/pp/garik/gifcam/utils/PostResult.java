package ua.pp.garik.gifcam.utils;

/**
 * Created by Vova on 31.03.2015.
 */
public class PostResult {
    private boolean result;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
