package ua.pp.garik.gifcam.utils;

import java.util.List;

import ua.pp.garik.gifcam.entity.Video;

public class FeedContainer {
    private List<Video> videos;

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }
}
