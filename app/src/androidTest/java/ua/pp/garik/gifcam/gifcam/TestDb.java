package ua.pp.garik.gifcam.gifcam;

import android.test.AndroidTestCase;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import ua.pp.garik.gifcam.entity.User;

/**
 * Created by root on 14.03.15.
 */
public class TestDb extends AndroidTestCase {
    private void RealmInsert() {
        Realm realm = Realm.getInstance(getContext());
        User user = new User();
        realm.beginTransaction();
        user.setId(1);
        user.setFirstName("kkkk");
        user.setLastName("mmmm");
        user.setFollowersCount(5);
        user.setFollowingCount(5);
        user.setRating(2);
        user.setPhoto("ssss");
        realm.commitTransaction();

    }

    public void testRealmRead() {
        RealmInsert();
        Realm realm = Realm.getInstance(getContext());
        RealmQuery<User> query = realm.where(User.class);
        RealmResults<User> results = query.findAll();
        for (User user : results) {
            assertEquals("kkkk", user.getFirstName());
        }
    }
}
